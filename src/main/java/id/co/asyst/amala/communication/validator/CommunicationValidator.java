/*
 * $Id$
 *
 * Copyright (c) 2018 Aero Systems Indonesia, PT.
 * All rights reserved.
 *
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
package id.co.asyst.amala.communication.validator;

import id.co.asyst.amala.communication.model.Communication;
import id.co.asyst.amala.core.exception.ValidationException;
import id.co.asyst.commons.core.payload.BaseParameter;
import id.co.asyst.commons.core.payload.Status;
import id.co.asyst.commons.core.validator.BaseValidator;
import id.co.asyst.commons.core.validator.ValidatorType;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Mar 27, 2019
 * @since 4.0
 */
@Component
public class CommunicationValidator extends BaseValidator<Communication> {

//    private final static int LENGHT_255 = 255;
//
//    public void validate(BaseParameter<Communication> parameter) {
//        Status status = validate(parameter, Communication.class);
//        if (status != null) {
//            throw new ValidationException(status);
//        }
//    }
//
//    public void validate(Communication communication, ValidatorType type) {
//        Calendar cal = Calendar.getInstance();
//        cal.set(Calendar.HOUR_OF_DAY, 0);
//        cal.set(Calendar.MINUTE, 0);
//        cal.set(Calendar.SECOND, 0);
//        cal.set(Calendar.MILLISECOND, 0);
//
//        if (!ValidatorType.CREATE.equals(type)) {
//            notBlank(communication.getId(), "id");
//            if (ValidatorType.DELETE.equals(type) || ValidatorType.RETRIEVEDETAIL.equals(type)) {
//                return;
//            }
//        }
//
//        checkReferenceNum(communication.getReferencenum());
//        checkTitle(communication.getTitle());
//        checkContent(communication.getContent());
//        checkEffectiveDate(communication.getEffectivedate());
//        if (ValidatorType.CREATE.equals(type)) {
//            if (communication.getEffectivedate().before(cal.getTime()))
//                throw new ValidationException(Status.INVALID(getMessage("effectivedate.earlier.today")));
//        }
//        checkIsAllGroup(communication.getIsallgroup());
//        checkDiscontinueDate(communication.getDiscontinuedate());
//        if (communication.getDiscontinuedate().before(communication.getEffectivedate()))
//            throw new ValidationException(Status.INVALID(getMessage("discontinuedate.earlier.effectivedate")));
//        if (communication.getDiscontinuedate().before(cal.getTime()))
//            throw new ValidationException(Status.INVALID(getMessage("discontinuedate.earlier.today")));
//        if (communication.getIsallgroup().equals(Boolean.FALSE)) {
//            // validate isallgroup 0
//            if (StringUtils.isEmpty(communication.getCommunicationRole()))
//                throw new ValidationException(Status.INVALID(getMessage("field.required", new Object[]{"group"})));
//            // validate isallgroup 0 and array kosong
//            if (communication.getCommunicationRole().size() == 0)
//                throw new ValidationException(Status.INVALID(getMessage("field.required", new Object[]{"group"})));
//        } else if (communication.getIsallgroup().equals(Boolean.TRUE)) {
//            // validate isallgroup 1
//            if (!StringUtils.isEmpty(communication.getCommunicationRole()))
//                throw new ValidationException(Status.INVALID(getMessage("communication.not.required")));
//        } else {
//            throw new ValidationException(Status.INVALID(getMessage("isallgroup.true.false")));
//        }
//    }
//
//    private void checkReferenceNum(String referencenum) {
//        notBlank(referencenum, "referencenum");
//        isMax(LENGHT_255, referencenum, "referencenum");
//    }
//
//    private void checkTitle(String title) {
//        notBlank(title, "title");
//        isMax(LENGHT_255, title, "title");
//        isAlphanumericSpace(title, "title");
//    }
//
//    private void checkContent(String content) {
//        notBlank(content, "content");
//    }
//
//    private void checkEffectiveDate(Date effectivedate) {
//        notNull(effectivedate, "effectivedate");
//    }
//
//    private void checkDiscontinueDate(Date discontinudate) {
//        notNull(discontinudate, "discontinuedate");
//    }
//
//    private void checkIsAllGroup(Boolean isallgroup) {
//        notNull(isallgroup, "isallgroup");
//    }
}
