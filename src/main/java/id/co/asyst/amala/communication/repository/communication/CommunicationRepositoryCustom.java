/*
 * $Id$
 *
 * Copyright (c) 2018 Aero Systems Indonesia, PT.
 * All rights reserved.
 *
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
package id.co.asyst.amala.communication.repository.communication;

import id.co.asyst.amala.communication.model.Communication;
import id.co.asyst.commons.core.payload.Paging;
import id.co.asyst.commons.core.payload.Parameter;
import id.co.asyst.commons.core.repository.RepositoryCustom;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Mar 27, 2019
 * @since 4.0
 */
public interface CommunicationRepositoryCustom extends RepositoryCustom<Communication> {

    long countCommunication(Parameter<Communication> parameter, Class<Communication> clasz, boolean like, boolean and);

    List<Communication> executeCustomSelectQueryCommunication(Parameter<Communication> parameter, Class<Communication> clasz, boolean like, boolean and);

    Page<Communication> executeCustomSelectQueryCommunication(Parameter<Communication> parameter, Paging paging, Class<Communication> clasz, boolean like, boolean and);
}
