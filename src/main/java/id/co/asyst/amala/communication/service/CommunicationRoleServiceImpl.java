/*
 * $Id$
 *
 * Copyright (c) 2018 Aero Systems Indonesia, PT.
 * All rights reserved.
 *
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
package id.co.asyst.amala.communication.service;

import id.co.asyst.amala.communication.model.CommunicationRole;
import id.co.asyst.amala.communication.repository.communicationrole.CommunicationRoleRepository;
import id.co.asyst.commons.core.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Mar 27, 2019
 * @since 4.0
 */
@Service
@Transactional
public class CommunicationRoleServiceImpl extends BaseService<CommunicationRole, String> implements CommunicationRoleService {

    @Autowired
    private CommunicationRoleRepository communicationRoleRepository;

    @Autowired
    public CommunicationRoleServiceImpl(CommunicationRoleRepository repository) {
        this.repository = repository;
        this.setRepository(repository);
        this.setCustomRepository(repository);
    }

    @Override
    public List<CommunicationRole> findByCommunicationid(String communicationid) {
        return communicationRoleRepository.findByCommunicationid(communicationid);
    }

    @Override
    public void deleteCommunicationRoleByCommunicationId(String id) {
        communicationRoleRepository.deleteCommunicationRoleByCommunicationId(id);
    }

    @Override
    public void saveCommunicationRoleByCommunicationId(CommunicationRole communicationRole) {
        save(communicationRole);
    }
}
