/*
 * $Id$
 *
 * Copyright (c) 2018 Aero Systems Indonesia, PT.
 * All rights reserved.
 *
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
package id.co.asyst.amala.communication.repository.communication;

import id.co.asyst.amala.communication.model.Communication;
import id.co.asyst.commons.core.payload.Paging;
import id.co.asyst.commons.core.payload.Parameter;
import id.co.asyst.commons.core.repository.BaseRepositoryCustom;
import id.co.asyst.commons.core.utils.ObjectUtils;
import id.co.asyst.commons.core.utils.RepositoryUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.persistence.Query;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Mar 27, 2019
 * @since 4.0
 */
public class CommunicationRepositoryCustomImpl extends BaseRepositoryCustom<Communication> implements CommunicationRepositoryCustom {
    @Override
    public long countCommunication(Parameter<Communication> parameter, Class<Communication> clasz, boolean like, boolean and) {
        Query query = entityManager.createQuery(createSelectCountQuery(parameter, clasz, like, and));
        return (long) query.getSingleResult();
    }

    @Override
    public List<Communication> executeCustomSelectQueryCommunication(Parameter<Communication> parameter, Class<Communication> clasz, boolean like, boolean and) {
        Query query = entityManager.createQuery(createSelectQuery(parameter, clasz, like, and));
        List<Communication> sourceList = query.getResultList();
        if (parameter != null) {
            List<String> columns = parameter.getColumn();
            if (columns != null && columns.size() > 0) {
                List<Communication> targetList = new ArrayList<Communication>();
                for (Communication source : sourceList) {
                    Communication target = BeanUtils.instantiateClass(clasz);
                    for (String column : columns) {
                        PropertyAccessorFactory.forBeanPropertyAccess(target).setPropertyValue(column, PropertyAccessorFactory.forBeanPropertyAccess(source).getPropertyValue(column));
                    }
                    targetList.add(target);
                }
                return targetList;
            }
        }
        return sourceList;
    }

    @Override
    public Page<Communication> executeCustomSelectQueryCommunication(Parameter<Communication> parameter, Paging paging, Class<Communication> clasz, boolean like, boolean and) {
        Query query = entityManager.createQuery(createSelectQuery(parameter, clasz, like, and));
        query = query.setFirstResult(RepositoryUtils.calculateOffset(paging.getPage(), paging.getLimit())).setMaxResults(paging.getLimit());
        List<Communication> sourceList = query.getResultList();

        Pageable pageable = PageRequest.of(paging.getPage() - 1, paging.getLimit());
        if (parameter != null) {
            List<String> columns = parameter.getColumn();
            if (columns != null && columns.size() > 0) {
                List<Communication> targetList = new ArrayList<Communication>();
                for (Communication source : sourceList) {
                    Communication target = BeanUtils.instantiateClass(clasz);
                    for (String column : columns) {
                        PropertyAccessorFactory.forBeanPropertyAccess(target).setPropertyValue(column,
                                PropertyAccessorFactory.forBeanPropertyAccess(source).getPropertyValue(column));
                    }
                    targetList.add(target);
                }
                return new PageImpl<Communication>(targetList, pageable, countCommunication(parameter, clasz, like, and));
            }
        }
        return new PageImpl<Communication>(sourceList, pageable, countCommunication(parameter, clasz, like, and));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private String createSelectQuery(Parameter<Communication> parameter, Class<Communication> clasz, boolean like, boolean and) {
        StringBuilder sb = new StringBuilder();
        sb.append("FROM ").append(clasz.getSimpleName());
        if (parameter != null) {
            sb.append(" a");
            // Criteria
            Map<String, String> criteria = ObjectUtils.cleanEmptyValue(parameter.getCriteria());
            if (criteria != null && criteria.size() > 0) {
                sb.append(" WHERE ");
                int i = 0;
                for (String key : criteria.keySet()) {
                    i++;
                    try {
                        String value = criteria.get(key).toUpperCase();
                        if (value.contains("&&&")) {
                            String[] values = value.split("&&&");
                            sb.append("(");
                            int j = 0;
                            for (String val : values) {
                                j++;
                                like = val.contains("%");
                                Field field = ObjectUtils.getField(clasz, key);
                                if (field.getType().equals(Integer.class) || field.getType().equals(Long.class) ||
                                        field.getType().equals(Double.class) || field.getType().equals(Float.class) ||
                                        field.getType().equals(Byte.class)) {
                                    if (like)
                                        sb.append("CONCAT(a.").append(key).append(",'') LIKE '").append(value).append("'");
                                    else
                                        sb.append("a.").append(key).append(" = '").append(value).append("'");
                                } else if (field.getType().equals(Date.class)) {
                                    if (key.equalsIgnoreCase("accessdate")) {
                                        sb.append("(a.effectivedate  <= '").append(criteria.get(key)).append("' AND '").append(criteria.get(key)).append("' <= a.discontinuedate)");
                                    } else {
                                        if (like)
                                            sb.append("a.").append(key).append(" LIKE '").append(val).append("'");
                                        else
                                            sb.append("a.").append(key).append(" = '").append(val).append("'");
                                    }
                                } else if (field.getType().equals(Boolean.class)) {
                                    sb.append("a.").append(key).append(" = ").append(val);
                                } else {
                                    if (like)
                                        sb.append("UPPER(a.").append(key).append(") LIKE '").append(val).append("'");
                                    else
                                        sb.append("UPPER(a.").append(key).append(") = '").append(val).append("'");

                                }
                                if (j < values.length) {
                                    sb.append(" OR ");
                                }
                            }
                            sb.append(")");
                        } else {
                            like = value.contains("%");
                            Field field = ObjectUtils.getField(clasz, key);
                            if (field.getType().equals(Integer.class) || field.getType().equals(Long.class) ||
                                    field.getType().equals(Double.class) || field.getType().equals(Float.class) ||
                                    field.getType().equals(Byte.class)) {
                                if (like)
                                    sb.append("CONCAT(a.").append(key).append(",'') LIKE '").append(value).append("'");
                                else
                                    sb.append("a.").append(key).append(" = '").append(value).append("'");
                            } else if (field.getType().equals(Date.class)) {
                                if (key.equalsIgnoreCase("accessdate")) {
                                    sb.append("(a.effectivedate  <= '").append(criteria.get(key)).append("' AND '").append(criteria.get(key)).append("' <= a.discontinuedate)");
                                } else {
                                    if (like)
                                        sb.append("a.").append(key).append(" LIKE '").append(value).append("'");
                                    else
                                        sb.append("a.").append(key).append(" = '").append(value).append("'");
                                }
                            } else if (field.getType().equals(Boolean.class)) {
                                sb.append("a.").append(key).append(" = ").append(value);
                            } else {
                                if (like)
                                    sb.append("UPPER(a.").append(key).append(") LIKE '").append(value).append("'");
                                else
                                    sb.append("UPPER(a.").append(key).append(") = '").append(value).append("'");
                            }
                        }
                        if (i < criteria.size()) {
                            if (and)
                                sb.append(" AND ");
                            else
                                sb.append(" OR ");
                        }
                    } catch (Exception e) {
                    }
                }
            }


            // Sort
            Map<String, String> sorts = ObjectUtils.cleanEmptyValue(parameter.getSort());
            if (sorts != null && sorts.size() > 0) {
                sb.append(" ORDER BY ");
                int i = 0;
                for (String key : sorts.keySet()) {
                    i++;
                    sb.append("a.").append(key).append(" ").append(sorts.get(key));
                    if (i < sorts.size())
                        sb.append(", ");
                }
            }
        }
        System.out.println("QUERY: " + sb.toString());
        return sb.toString();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private String createSelectCountQuery(Parameter<Communication> parameter, Class<Communication> clasz, boolean like, boolean and) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT COUNT(*) FROM ").append(clasz.getSimpleName());
        if (parameter != null) {
            sb.append(" a");
            // Criteria
            Map<String, String> criteria = ObjectUtils.cleanEmptyValue(parameter.getCriteria());
            if (criteria != null && criteria.size() > 0) {
                sb.append(" WHERE ");
                int i = 0;
                for (String key : criteria.keySet()) {
                    i++;
                    try {
                        String value = criteria.get(key).toUpperCase();
                        if (value.contains("&&&")) {
                            String[] values = value.split("&&&");
                            sb.append("(");
                            int j = 0;
                            for (String val : values) {
                                j++;
                                like = val.contains("%");
                                Field field = ObjectUtils.getField(clasz, key);
                                if (field.getType().equals(Integer.class) || field.getType().equals(Long.class) ||
                                        field.getType().equals(Double.class) || field.getType().equals(Float.class) ||
                                        field.getType().equals(Byte.class)) {
                                    if (like)
                                        sb.append("CONCAT(a.").append(key).append(",'') LIKE '").append(value).append("'");
                                    else
                                        sb.append("a.").append(key).append(" = '").append(value).append("'");
                                } else if (field.getType().equals(Date.class)) {
                                    if (key.equalsIgnoreCase("accessdate")) {
                                        sb.append("(a.effectivedate  <= '").append(criteria.get(key)).append("' AND '").append(criteria.get(key)).append("' <= a.discontinuedate)");
                                    } else {
                                        if (like)
                                            sb.append("a.").append(key).append(" LIKE '").append(val).append("'");
                                        else
                                            sb.append("a.").append(key).append(" = '").append(val).append("'");
                                    }
                                } else if (field.getType().equals(Boolean.class)) {
                                    sb.append("a.").append(key).append(" = ").append(val);
                                } else {
                                    if (like)
                                        sb.append("UPPER(a.").append(key).append(") LIKE '").append(val).append("'");
                                    else
                                        sb.append("UPPER(a.").append(key).append(") = '").append(val).append("'");
                                }
                                if (j < values.length) {
                                    sb.append(" OR ");
                                }
                            }
                            sb.append(")");
                        } else {
                            like = value.contains("%");
                            Field field = ObjectUtils.getField(clasz, key);
                            if (field.getType().equals(Integer.class) || field.getType().equals(Long.class) ||
                                    field.getType().equals(Double.class) || field.getType().equals(Float.class) ||
                                    field.getType().equals(Byte.class)) {
                                if (like)
                                    sb.append("CONCAT(a.").append(key).append(",'') LIKE '").append(value).append("'");
                                else
                                    sb.append("a.").append(key).append(" = '").append(value).append("'");
                            } else if (field.getType().equals(Date.class)) {
                                if (key.equalsIgnoreCase("accessdate")) {
                                    sb.append("(a.effectivedate  <= '").append(criteria.get(key)).append("' AND '").append(criteria.get(key)).append("' <= a.discontinuedate)");
                                } else {
                                    if (like)
                                        sb.append("a.").append(key).append(" LIKE '").append(value).append("'");
                                    else
                                        sb.append("a.").append(key).append(" = '").append(value).append("'");
                                }
                            } else if (field.getType().equals(Boolean.class)) {
                                sb.append("a.").append(key).append(" = ").append(value);
                            } else {
                                if (like)
                                    sb.append("UPPER(a.").append(key).append(") LIKE '").append(value).append("'");
                                else
                                    sb.append("UPPER(a.").append(key).append(") = '").append(value).append("'");
                            }
                        }
                        if (i < criteria.size()) {
                            if (and)
                                sb.append(" AND ");
                            else
                                sb.append(" OR ");
                        }
                    } catch (Exception e) {
                    }
                }
            }

        }
        return sb.toString();
    }
}
