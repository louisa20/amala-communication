/*
 * $Id$
 *
 * Copyright (c) 2018 Aero Systems Indonesia, PT.
 * All rights reserved.
 *
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
package id.co.asyst.amala.communication.repository.communication;

import id.co.asyst.amala.communication.model.Communication;
import id.co.asyst.commons.core.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Mar 27, 2019
 * @since 4.0
 */
@Repository
public interface CommunicationRepository extends BaseRepository<Communication, String>, CommunicationRepositoryCustom {
    @Query("from Communication a where UPPER(a.referencenum) = UPPER(:referencenum)")
    Communication findByReferencenum(@Param("referencenum") String referencenum);
}
