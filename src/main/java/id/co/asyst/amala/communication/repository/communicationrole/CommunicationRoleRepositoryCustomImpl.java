/*
 * $Id$
 *
 * Copyright (c) 2018 Aero Systems Indonesia, PT.
 * All rights reserved.
 *
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
package id.co.asyst.amala.communication.repository.communicationrole;

import id.co.asyst.amala.communication.model.CommunicationRole;
import id.co.asyst.commons.core.repository.BaseRepositoryCustom;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Mar 27, 2019
 * @since 4.0
 */
@Repository
@Transactional
public class CommunicationRoleRepositoryCustomImpl extends BaseRepositoryCustom<CommunicationRole> implements CommunicationRoleRepositoryCustom {
}
