/*
 * $Id$
 *
 * Copyright (c) 2018 Aero Systems Indonesia, PT.
 * All rights reserved.
 *
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
package id.co.asyst.amala.communication.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.asyst.amala.communication.model.Communication;
import id.co.asyst.amala.communication.model.CommunicationRole;
import id.co.asyst.amala.communication.service.CommunicationService;
import id.co.asyst.amala.communication.validator.CommunicationValidator;
import id.co.asyst.amala.core.exception.ValidationException;
//import id.co.asyst.amala.core.validator.ValidatorType;
import id.co.asyst.commons.core.controller.BaseController;
import id.co.asyst.commons.core.payload.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Mar 27, 2019
 * @since 4.0
 */

@SuppressWarnings({"MVCPathVariableInspection", "DuplicatedCode"})
@CrossOrigin(origins = "*")
@RestController
public class CommunicationController extends BaseController {

    private static final long serialVersionUID = -7050960033710998119L;
    private static Logger logger = LogManager.getLogger(CommunicationController.class);

    private CommunicationService service;
    private CommunicationValidator validator;

    @Autowired
    public CommunicationController(CommunicationService service, CommunicationValidator validator){
        this.service = service;
        this.validator = validator;
    }

//    @Value("http://172.25.230.123:40010/lms/user/role/detail/v2.1/retrieveroledetail")
//    private String urlDetailRole;
//
//    @Value("http://172.25.230.123:40001/lms/master/filestorage/create/v1")
//    private String urlCreateFileStorage;

//    @PostMapping("${controller.mapping.operation.create}")
//    public BaseResponse<Communication> create(@RequestParam(value = "file") MultipartFile[] files, @RequestParam(value = "request") String request, @RequestParam(value = "path", required = false) String path) {
//        BaseResponse<Communication> response = new BaseResponse<>();
//        try {
//            communicationService.filevalidate(files);
//            BaseRequest<BaseParameter<Communication>> communicationrequest = communicationService.communicationRequestFormData(request);
//            BaseRequest<BaseParameter<Communication>> comReq = new BaseRequest<>();
//            comReq = new ObjectMapper().convertValue(communicationrequest, comReq.getClass());
//            BaseParameter<Communication> communicationParameter = new BaseParameter<>();
//            communicationParameter = new ObjectMapper().convertValue(communicationrequest.getParameter(), communicationParameter.getClass());
//            Communication communication = new ObjectMapper().convertValue(communicationParameter.getData(), Communication.class);
//            validator.validate(communication, ValidatorType.CREATE);
//            response.setIdentity(communicationrequest.getIdentity());
//            Communication communicationSave = communicationService.save(comReq, files, path);
//            if (communication.getCommunicationRole() != null) {
//                communication.setId(communicationSave.getId());
//                communication.setImage(communicationSave.getImage());
//                communication.setCreatedDate(communicationSave.getCreatedDate());
//                communication.setCreatedBy(communicationSave.getCreatedBy());
//                communication.setActive(communicationSave.getActive());
//                response.setResult(communicationService.retrieveCreateAndUpdate(communication, communicationrequest.getIdentity()));
//            } else {
//                response.setResult(communicationSave);
//            }
//            response.setStatus(Status.SUCCESS(getMessage("data.create.success", new Object[]{"News"})));
//        } catch (ValidationException ve) {
//            if (ve.getStatus() == null) {
//                response.setStatus(Status.INVALID(getMessage(ve.getKey(), new Object[]{ve.getModuleName(), ve.getParameter()})));
//            } else {
//                response.setStatus(ve.getStatus());
//            }
//        } catch (DataIntegrityViolationException he) {
//            response.setStatus(new Status(Status.ERROR_INVALID_DATA_CODE, Status.ERROR_INVALID_DATA_DESC, getMessage("data.constraint.violation")));
//        } catch (Exception e) {
//            logger.error(e.getMessage(), e);
//            response.setStatus(Status.ERROR(e.getMessage()));
//        }
//        return response;
//    }

//    @PostMapping("${controller.mapping.operation.update}")
//    public BaseResponse<Communication> update(@RequestParam(value = "request", required = false) String request, @RequestParam(value = "path", required = false) String path, @RequestParam(value = "file", required = false) MultipartFile[] files) {
//        BaseResponse<Communication> response = new BaseResponse<>();
//        try {
//            BaseRequest<BaseParameter<Communication>> communicationrequest = communicationService.communicationRequestFormData(request);
//            BaseRequest<BaseParameter<Communication>> comReq = new BaseRequest<>();
//            comReq = new ObjectMapper().convertValue(communicationrequest, comReq.getClass());
//            BaseParameter<Communication> communicationParameter = new BaseParameter<>();
//            communicationParameter = new ObjectMapper().convertValue(communicationrequest.getParameter(), communicationParameter.getClass());
//            Communication communication = new ObjectMapper().convertValue(communicationParameter.getData(), Communication.class);
//            validator.validate(communication, ValidatorType.UPDATE);
//            response.setIdentity(communicationrequest.getIdentity());
//            Communication communicationUpdate = communicationService.update(comReq, files, path);
//            if (communication.getCommunicationRole() != null) {
//                response.setResult(communicationService.retrieveCreateAndUpdate(communication, communicationrequest.getIdentity()));
//            } else {
//                response.setResult(communicationUpdate);
//            }
//            response.setStatus(Status.SUCCESS(getMessage("data.update.success", new Object[]{"News"})));
//        } catch (ValidationException ve) {
//            if (ve.getStatus() == null) {
//                response.setStatus(Status.INVALID(getMessage(ve.getKey(), new Object[]{ve.getModuleName(), ve.getParameter()})));
//            } else {
//                response.setStatus(ve.getStatus());
//            }
//        } catch (DataIntegrityViolationException he) {
//            response.setStatus(new Status(Status.ERROR_INVALID_DATA_CODE, Status.ERROR_INVALID_DATA_DESC, getMessage("data.constraint.violation")));
//        } catch (Exception e) {
//            logger.error(e.getMessage(), e);
//            response.setStatus(Status.ERROR(e.getMessage()));
//        }
//        return response;
//    }
//
//    @PostMapping("${controller.mapping.operation.retrievedetail}")
//    public BaseResponse<Communication> retrievedetail(@RequestBody BaseRequest<BaseParameter<Communication>> request) {
//        BaseResponse<Communication> response = new BaseResponse<>();
//        try {
//            validator.validate(request.getParameter().getData(), ValidatorType.RETRIEVEDETAIL);
//            response.setIdentity(request.getIdentity());
//            response.setResult(communicationService.retrievedetail(request));
//            response.setStatus(Status.SUCCESS());
//        } catch (ValidationException ve) {
//            if (ve.getStatus() == null) {
//                response.setStatus(Status.INVALID(getMessage(ve.getKey(), new Object[]{ve.getModuleName(), ve.getParameter()})));
//            } else {
//                response.setStatus(ve.getStatus());
//            }
//        } catch (DataIntegrityViolationException he) {
//            response.setStatus(new Status(Status.ERROR_INVALID_DATA_CODE, Status.ERROR_INVALID_DATA_DESC, getMessage("data.constraint.violation")));
//        } catch (Exception e) {
//            logger.error(e.getMessage(), e);
//            response.setStatus(Status.ERROR(e.getMessage()));
//        }
//        return response;
//    }
//
    @PostMapping("${controller.mapping.operation.delete}")
    public BaseResponse<Communication> delete(@RequestBody BaseRequest<BaseParameter<Communication>> request) {
        BaseResponse<Communication> response = new BaseResponse<>();
//        try {
//            validator.validate(request.getParameter().getData(), ValidatorType.RETRIEVEDETAIL);
//            response.setIdentity(request.getIdentity());
//            communicationService.doSomeThingToCommunicationRole(request, ValidatorType.DELETE);
//            communicationService.delete(request);
//            response.setStatus(Status.SUCCESS(getMessage("data.delete.success", new Object[]{"News"})));
//        } catch (ValidationException ve) {
//            if (ve.getStatus() == null) {
//                response.setStatus(Status.INVALID(getMessage(ve.getKey(), new Object[]{ve.getModuleName(), ve.getParameter()})));
//            } else {
//                response.setStatus(ve.getStatus());
//            }
//        } catch (DataIntegrityViolationException he) {
//            response.setStatus(new Status(Status.ERROR_INVALID_DATA_CODE, Status.ERROR_INVALID_DATA_DESC, getMessage("data.constraint.violation")));
//        } catch (Exception e) {
//            logger.error(e.getMessage(), e);
//            response.setStatus(Status.ERROR(e.getMessage()));
//        }
        return response;
        // can't delete data
    }
//
//    @PostMapping("${controller.mapping.operation.retrieve}")
//    public BaseResponse<List<Communication>> retrieve(@RequestBody BaseRequest<BaseParameter<Communication>> request) {
//        BaseResponse<List<Communication>> response = new BaseResponse<>();
//        try {
//            BaseParameter<Communication> parameter = request.getParameter();
//            validator.validate(parameter);
//            response.setIdentity(request.getIdentity());
//            Paging paging = (request.getPaging() == null) ? Paging.initialize() : Paging.validate(request.getPaging());
//            List<Communication> communicationList = null;
//            if (paging.getLimit() < 0) {
//                if (parameter != null) {
//                    if (parameter.getCriteria().containsKey("accessdate")) {
//                        communicationList = communicationService.executeCustomSelectQueryCommunication(parameter, Communication.class, true, true);
//                    } else {
//                        communicationList = communicationService.executeCustomSelectQuery(parameter, Communication.class, true, true);
//                    }
//                } else
//                    communicationList = communicationService.findAll();
//            } else {
//                Page<Communication> communicationPage = null;
//                if (parameter != null) {
//                    if (parameter.getCriteria().containsKey("accessdate")) {
//                        communicationPage = communicationService.executeCustomSelectQueryCommunication(parameter, paging, Communication.class, true, true);
//                    } else {
//                        communicationPage = communicationService.executeCustomSelectQuery(parameter, paging, Communication.class, true, true);
//                    }
//                    communicationList = communicationPage.getContent();
//                } else {
//                    communicationPage = communicationService.findAll(paging);
//                    communicationList = communicationPage.getContent();
//                }
//                paging.setTotalpage(communicationPage.getTotalPages());
//                paging.setTotalrecord(communicationPage.getTotalElements());
//                response.setPaging(paging);
//            }
//            if (communicationList.size() > 0) {
//                for (Communication communication : communicationList) {
//                    for (CommunicationRole communicationRole : communication.getCommunicationRole()) {
//                        String rolename = communicationService.roleName(request.getIdentity(), communicationRole);
//                        communicationRole.setRolename(rolename);
//                    }
//                }
//            }
//            response.setResult(communicationList);
//            response.setStatus(Status.SUCCESS());
//        } catch (ValidationException ve) {
//            if (ve.getStatus() == null) {
//                response.setStatus(Status.INVALID(getMessage(ve.getKey(), new Object[]{ve.getModuleName(), ve.getParameter()})));
//            } else {
//                response.setStatus(ve.getStatus());
//            }
//        } catch (DataIntegrityViolationException he) {
//            response.setStatus(new Status(Status.ERROR_INVALID_DATA_CODE, Status.ERROR_INVALID_DATA_DESC, getMessage("data.constraint.violation")));
//        } catch (Exception e) {
//            logger.error(e.getMessage(), e);
//            response.setStatus(Status.ERROR(e.getMessage()));
//        }
//        return response;
//    }
//
//    @PostMapping("${controller.mapping.operation.activate}")
//    public BaseResponse<Communication> activate(@RequestBody BaseRequest<BaseParameter<Communication>> request) {
//        BaseResponse<Communication> response = new BaseResponse<>();
//        try {
//            validator.validate(request.getParameter().getData(), ValidatorType.DELETE);
//            response.setIdentity(request.getIdentity());
//            communicationService.activate(request);
//            response.setStatus(Status.SUCCESS(getMessage("data.activate.success")));
//        } catch (ValidationException ve) {
//            if (ve.getStatus() == null) {
//                response.setStatus(Status.INVALID(getMessage(ve.getKey(), new Object[]{ve.getModuleName(), ve.getParameter()})));
//            } else {
//                response.setStatus(ve.getStatus());
//            }
//        } catch (DataIntegrityViolationException he) {
//            response.setStatus(new Status(Status.ERROR_INVALID_DATA_CODE, Status.ERROR_INVALID_DATA_DESC, getMessage("data.constraint.violation")));
//        } catch (Exception e) {
//            logger.error(e.getMessage(), e);
//            response.setStatus(Status.ERROR(e.getMessage()));
//        }
//        return response;
//        // can't use
//    }
//
//    @PostMapping("${controller.mapping.operation.deactivate}")
//    public BaseResponse<Communication> deactive(@RequestBody BaseRequest<BaseParameter<Communication>> request) {
//        BaseResponse<Communication> response = new BaseResponse<>();
//        try {
//            validator.validate(request.getParameter().getData(), ValidatorType.DELETE);
//            response.setIdentity(request.getIdentity());
//            communicationService.deactivate(request);
//            response.setStatus(Status.SUCCESS(getMessage("data.deactivate.success")));
//        } catch (ValidationException ve) {
//            if (ve.getStatus() == null) {
//                response.setStatus(Status.INVALID(getMessage(ve.getKey(), new Object[]{ve.getModuleName(), ve.getParameter()})));
//            } else {
//                response.setStatus(ve.getStatus());
//            }
//        } catch (DataIntegrityViolationException he) {
//            response.setStatus(new Status(Status.ERROR_INVALID_DATA_CODE, Status.ERROR_INVALID_DATA_DESC, getMessage("data.constraint.violation")));
//        } catch (Exception e) {
//            logger.error(e.getMessage(), e);
//            response.setStatus(Status.ERROR(e.getMessage()));
//        }
//        return response;
//        // can't use
//    }
}
