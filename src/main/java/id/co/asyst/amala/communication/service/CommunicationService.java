/*
 * $Id$
 *
 * Copyright (c) 2018 Aero Systems Indonesia, PT.
 * All rights reserved.
 *
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
package id.co.asyst.amala.communication.service;

import id.co.asyst.amala.communication.model.Communication;
import id.co.asyst.amala.communication.model.CommunicationRole;
import id.co.asyst.commons.core.payload.*;
import id.co.asyst.commons.core.service.Service;
import id.co.asyst.commons.core.validator.ValidatorType;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Mar 27, 2019
 * @since 4.0
 */
public interface CommunicationService extends Service<Communication, String> {

//    List<Communication> executeCustomSelectQueryCommunication(Parameter<Communication> parameter, Class<Communication> clasz, boolean like, boolean and);
//
//    Page<Communication> executeCustomSelectQueryCommunication(Parameter<Communication> parameter, Paging paging, Class<Communication> clasz, boolean like, boolean and);
//
//    Communication save(BaseRequest<BaseParameter<Communication>> request, MultipartFile[] files, String path);
//
//    Communication update(BaseRequest<BaseParameter<Communication>> request, MultipartFile[] files, String path);
//
//    BaseRequest<BaseParameter<Communication>> communicationRequestFormData(String request);
//
//    void filevalidate(MultipartFile[] files);
//
//    void delete(BaseRequest<BaseParameter<Communication>> request);
//
//    void activate(BaseRequest<BaseParameter<Communication>> request);
//
//    void deactivate(BaseRequest<BaseParameter<Communication>> request);
//
//    Communication retrievedetail(BaseRequest<BaseParameter<Communication>> request);
//
//    Communication findbyid(String communicationid);
//
//    Communication doSomeThingToCommunicationRole(BaseRequest<BaseParameter<Communication>> request, ValidatorType validate);
//
//    String roleName(Identity identity, CommunicationRole communicationRole);
//
//    Communication retrieveCreateAndUpdate(Communication communication, Identity identity);
}
