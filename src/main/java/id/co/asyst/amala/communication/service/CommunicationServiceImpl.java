/*
 * $Id$
 *
 * Copyright (c) 2018 Aero Systems Indonesia, PT.
 * All rights reserved.
 *
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
package id.co.asyst.amala.communication.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.asyst.amala.communication.model.Communication;
import id.co.asyst.amala.communication.model.CommunicationRole;
import id.co.asyst.amala.communication.model.CommunicationRoleId;
import id.co.asyst.amala.communication.repository.communication.CommunicationRepository;
import id.co.asyst.amala.communication.repository.communicationrole.CommunicationRoleRepository;
import id.co.asyst.amala.core.exception.ValidationException;
//import id.co.asyst.amala.core.validator.ValidatorType;
//import id.co.asyst.amala.user.model.Role;
import id.co.asyst.commons.core.payload.*;
import id.co.asyst.commons.core.service.BaseService;
import id.co.asyst.commons.core.utils.GeneratorUtils;
//import id.co.asyst.commons.files.service.RemoteStorageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Mar 27, 2019
 * @since 4.0
 */
@Service
@Transactional
public class CommunicationServiceImpl extends BaseService<Communication, String> implements CommunicationService {

//    @Autowired
//    private CommunicationRepository communicationRepository;
//
//    Logger logger = LogManager.getLogger();
//
//    @Autowired
//    private ExternalService externalService;
//
//    private String delete = "DELETE";
//    private String deactive = "DEACTIVATE";
//    private String active = "ACTIVATE";
//    private String retrieve = "RETRIEVE";
//    private String retrievedetail = "RETRIEVEDETAIL";
//
//    @Value("${storage.remote.ftp.host}")
//    private String port;
//
//    @Autowired
//    public CommunicationServiceImpl(CommunicationRepository repository) {
//        this.repository = repository;
//        this.setRepository(repository);
//        this.setCustomRepository(repository);
//    }
//
//    @Autowired
//    private RemoteStorageService sftpService;
//
//    @Autowired
//    private CommunicationRoleRepository communicationRoleRepository;
//
//    @Autowired
//    private CommunicationRoleService communicationRoleService;
//
//    @Override
//    public List<Communication> executeCustomSelectQueryCommunication(Parameter<Communication> parameter, Class<Communication> clasz, boolean like, boolean and) {
//        return communicationRepository.executeCustomSelectQueryCommunication(parameter, clasz, like, and);
//    }
//
//    @Override
//    public Page<Communication> executeCustomSelectQueryCommunication(Parameter<Communication> parameter, Paging paging, Class<Communication> clasz, boolean like, boolean and) {
//        return communicationRepository.executeCustomSelectQueryCommunication(parameter, paging, clasz, like, and);
//    }
//
//    @Override
//    public Communication save(BaseRequest<BaseParameter<Communication>> request, MultipartFile[] files, String path) {
//        BaseParameter parameter = new ObjectMapper().convertValue(request.getParameter(), BaseParameter.class);
//        Communication communication = new ObjectMapper().convertValue(parameter.getData(), Communication.class);
//        communication.setCreatedBy(request.getIdentity().getUserid());
//        Date date = new Date();
//        String id = GeneratorUtils.GenerateId("", date, 3);
//        communication.setId(id);
//        checkCommunication(communication.getId(), communication.getReferencenum(), ValidatorType.CREATE);
//        communication.setImage(uploadFile(files, path, communication));
//        isAllGroup(communication, request.getIdentity());
//        communication.setActive(true);
//        return super.save(communication);
//    }
//
//    @Override
//    public Communication update(BaseRequest<BaseParameter<Communication>> request, MultipartFile[] files, String path) {
//        BaseParameter parameter = new ObjectMapper().convertValue(request.getParameter(), BaseParameter.class);
//        Communication communication = new ObjectMapper().convertValue(parameter.getData(), Communication.class);
//        Communication communicationDB = checkCommunication(communication.getId(), communication.getReferencenum(), ValidatorType.UPDATE);
//        if (files.length != 0) {
//            communicationDB.setImage(uploadFile(files, path, communicationDB));
//        }
//        isAllGroup(communication, request.getIdentity());
//        if (communication.getIsallgroup()) {
//            communicationDB.getCommunicationRole().clear();
//        } else {
//            communicationRoleService.deleteCommunicationRoleByCommunicationId(communication.getId());
//            for (CommunicationRole communicationRole : communication.getCommunicationRole()) {
//                communicationRoleService.saveCommunicationRoleByCommunicationId(communicationRole);
//            }
//        }
//        communicationDB.setTitle(communication.getTitle());
//        communicationDB.setReferencenum(communication.getReferencenum());
//        communicationDB.setContent(communication.getContent());
//        if (!StringUtils.isEmpty(communication.getDiscontinuedate()))
//            communicationDB.setEffectivedate(communication.getEffectivedate());
//        communicationDB.setDiscontinuedate(communication.getDiscontinuedate());
//        communicationDB.setUpdatedBy(request.getIdentity().getUserid());
//        communicationDB.setIsallgroup(communication.getIsallgroup());
//        return super.update(communicationDB);
//    }
//
//    @Override
//    public void filevalidate(MultipartFile[] files) {
//        if (files.length == 0) {
//            throw new ValidationException(Status.INVALID(getMessage("field.required", new Object[]{"file"})));
//        }
//    }
//
//    @Override
//    public void delete(BaseRequest<BaseParameter<Communication>> request) {
//        try {
//            delete(request.getParameter().getData().getId());
//        } catch (Exception e) {
//            throw new ValidationException("data.not.found", "News", request.getParameter().getData().getId());
//        }
//    }
//
//    @Override
//    public void activate(BaseRequest<BaseParameter<Communication>> request) {
//        Communication communicationDB = checkCommunicationNotCreateUpdate(request.getParameter().getData(), request.getIdentity(), active);
//        doSomeThingToCommunicationRole(request, ValidatorType.ACTIVATE);
//        communicationDB.setActive(true);
//        communicationDB.setUpdatedBy(request.getIdentity().getUserid());
//        super.update(communicationDB);
//    }
//
//    @Override
//    public void deactivate(BaseRequest<BaseParameter<Communication>> request) {
//        Communication communicationDB = checkCommunicationNotCreateUpdate(request.getParameter().getData(), request.getIdentity(), deactive);
//        doSomeThingToCommunicationRole(request, ValidatorType.DEACTIVATE);
//        communicationDB.setActive(false);
//        communicationDB.setUpdatedBy(request.getIdentity().getUserid());
//        super.update(communicationDB);
//    }
//
//    @Override
//    public Communication retrievedetail(BaseRequest<BaseParameter<Communication>> request) {
//        return checkCommunicationNotCreateUpdate(request.getParameter().getData(), request.getIdentity(), retrievedetail);
//    }
//
//    @Override
//    public Communication findbyid(String communicationid) {
//        return repository.findById(communicationid).orElse(null);
//    }
//
//    @Override
//    public BaseRequest<BaseParameter<Communication>> communicationRequestFormData(String request) {
//        BaseRequest<BaseParameter<Communication>> communicationRequest = new BaseRequest<>();
//        if (request != null) {
//            try {
//                communicationRequest = new ObjectMapper().readValue(request, communicationRequest.getClass());
//            } catch (Exception e) {
//                throw new ValidationException(Status.INVALID(getMessage("invalid.data.request")));
//            }
//        } else {
//            throw new ValidationException(Status.INVALID(getMessage("field.required", new Object[]{"request"})));
//        }
//        return communicationRequest;
//    }
//
//    private String uploadFile(MultipartFile[] files, String path, Communication communication) {
//        String fileDownloadUrl = null;
//        for (MultipartFile file : files) {
//            String filename;
//            if (file != null && file.getContentType() != null) {
//                if (!file.getContentType().equals("image/png") && !file.getContentType().equals("image/jpg") && !file.getContentType().equals("image/jpeg")) {
//                    throw new ValidationException(Status.INVALID("file upload detected PNG/JPG/JPEG"));
//                }
//            } else {
//                throw new ValidationException(Status.INVALID("File is Null"));
//            }
//            filename = file.getOriginalFilename();
//
//            int extension = filename.lastIndexOf(".");
//            String extensionfile = "." + filename.substring(extension + 1);
//            String setImageName = StringUtils.cleanPath(communication.getId() + filename.replaceAll(" ", "_"));
//            // set data filename
//            if (!StringUtils.isEmpty(path)) {
//                fileDownloadUrl = "http://" + port + ":3000" + path + "/" + setImageName;
//            } else {
//                fileDownloadUrl = "http://" + port + ":3000" + "/" + setImageName;
//            }
//            try {
//                if (StringUtils.isEmpty(path)) {
//                    sftpService.put(setImageName, file.getInputStream());
//                } else {
//                    sftpService.put(setImageName, file.getInputStream(), path);
//                }
//            } catch (IOException io) {
//                io.printStackTrace();
//            }
//        }
//        return fileDownloadUrl;
//    }
//
//    private Communication checkCommunicationNotCreateUpdate(Communication communication, Identity identity, String validate) {
//        Date date = null;
//        try {
//            date = new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
//            logger.info(date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        Communication communicationDB = repository.findById(communication.getId()).orElse(null);
//        if (communicationDB != null) {
//            if (!validate.equals(retrieve) && !validate.equals(retrievedetail) && !validate.equals(delete)) {
//                if (communicationDB.getEffectivedate().before(date) && communicationDB.getDiscontinuedate().before(date)) {
//                    throw new ValidationException("data.expired", "News", communication.getId());
//                } else if (communicationDB.getEffectivedate().after(date) && communicationDB.getDiscontinuedate().after(date)) {
//                    throw new ValidationException("data.not.ready", "News", communication.getId());
//                }
//                if (validate.equals(active)) {
//                    if (communicationDB.getActive().equals(true)) {
//                        throw new ValidationException(Status.INVALID(getMessage("data.active")));
//                    }
//                } else if (validate.equals(deactive)) {
//                    if (communicationDB.getActive().equals(false)) {
//                        throw new ValidationException(Status.INVALID(getMessage("data.not.active")));
//                    }
//                }
//            }
//            return communicationDB;
//        } else {
//            throw new ValidationException("data.not.found", "News", communication.getId());
//        }
//    }
//
//    private Communication checkCommunication(String communicationid, String referencenum, ValidatorType type) {
//        Date date = new Date();
//        try {
//            date = new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
//            logger.info(date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        Communication communicationDB = repository.findById(communicationid).orElse(null);
//        if (!ValidatorType.CREATE.equals(type)) {
//            if (communicationDB != null) {
//                if (ValidatorType.UPDATE.equals(type)) {
//                    if (communicationDB.getActive().equals(false)) {
//                        throw new ValidationException("data.not.active", "News", communicationid);
//                    }
//                    if (communicationDB.getEffectivedate().before(date) && communicationDB.getDiscontinuedate().before(date)) {
//                        throw new ValidationException("data.expired", "News", communicationid);
//                    }
////                    if (communicationDB.getEffectivedate().after(date) && communicationDB.getDiscontinuedate().after(date)) {
////                        throw new ValidationException("data.not.ready", "News", communicationid);
////                    }
//                    Communication communicationByEnum = communicationRepository.findByReferencenum(referencenum);
//                    if (communicationByEnum != null && communicationByEnum.getId() != communicationid) {
//                        throw new ValidationException(Status.DATA_ALREADY_EXIST(Status.ERROR_EXIST_DESC));
//                    }
//                }
//                return communicationDB;
//            } else {
//                throw new ValidationException("data.not.found", "News", communicationid);
//            }
//        } else {
//            communicationDB = communicationRepository.findByReferencenum(referencenum);
//            if (communicationDB != null) {
//                throw new ValidationException(Status.DATA_ALREADY_EXIST(Status.ERROR_EXIST_DESC));
//            }
//            return null;
//        }
//    }
//
//    private Communication isAllGroup(Communication communication, Identity identity) {
//        if (communication.getIsallgroup().equals(false)) {
//            for (CommunicationRole communicationRole : communication.getCommunicationRole()) {
//                List<Role> role = externalService.checkRoleAvailability(communicationRole, identity);
//                if (role.size() == 0) {
//                    throw new ValidationException("data.not-found", "rolecode", communicationRole.getRolecode());
//                }
//                communicationRole.setActive(true);
//                communicationRole.setCreatedBy(identity.getUserid());
//                //TODO: recheck later when role is developed
//                //communicationRole.setRolecode(dataRole.getRolecode());
//                Date date = null;
//                try {
//                    date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//                logger.info(" " + date);
//                communicationRole.setCreatedDate(date);
//                communicationRole.setCommunicationid(communication.getId());
//            }
//        }
//        return communication;
//    }
//
//    public Communication doSomeThingToCommunicationRole(BaseRequest<BaseParameter<Communication>> request, ValidatorType validate) {
//        checkCommunicationNotCreateUpdate(request.getParameter().getData(), request.getIdentity(), delete);
//        Date date = null;
//        try {
//            date = new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
//            logger.info(date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        Communication communication = request.getParameter().getData();
//        if (validate.equals(ValidatorType.DELETE)) {
//            List<CommunicationRole> communicationRoleList = communicationRoleRepository.findByCommunicationid(communication.getId());
//            if (communicationRoleList.size() > 0) {
//                communicationRoleRepository.deleteCommunicationRoleByCommunicationId(communication.getId());
//            } else {
//                return null;
//            }
//        } else if (validate.equals(ValidatorType.ACTIVATE)) {
//            List<CommunicationRole> communicationRoleList = communicationRoleRepository.findByCommunicationid(communication.getId());
//            if (communicationRoleList.size() > 0) {
//                for (CommunicationRole communicationRole : communicationRoleList) {
//                    communicationRole.setActive(true);
//                    communicationRole.setCreatedBy(request.getIdentity().getUserid());
//                    communicationRole.setCreatedDate(date);
//                    communicationRoleRepository.saveAndFlush(communicationRole);
//                }
//            } else {
//                return null;
//            }
//        } else if (validate.equals(ValidatorType.DEACTIVATE)) {
//            List<CommunicationRole> communicationRoleList = communicationRoleRepository.findByCommunicationid(communication.getId());
//            if (communicationRoleList.size() > 0) {
//                for (CommunicationRole communicationRole : communicationRoleList) {
//                    communicationRole.setActive(false);
//                    communicationRole.setCreatedBy(request.getIdentity().getUserid());
//                    communicationRole.setCreatedDate(date);
//                    communicationRoleRepository.saveAndFlush(communicationRole);
//                }
//            } else {
//                return null;
//            }
//        }
//        return null;
//    }
//
//    @Override
//    public String roleName(Identity identity, CommunicationRole communicationRole) {
//        List<Role> roleDB = externalService.checkRoleAvailability(communicationRole, identity);
//        if (roleDB.size() > 0) {
//            Role role = roleDB.get(0);
//            return role.getRolename();
//        } else {
//            return "";
//        }
//    }
//
//    @Override
//    public Communication retrieveCreateAndUpdate(Communication communication, Identity identity) {
//        if (communication.getIsallgroup().equals(Boolean.FALSE)) {
//            Set<CommunicationRole> communicationRoleList = new HashSet<>();
//            Communication communicationRetrieve = communicationRepository.findById(communication.getId()).orElse(null);
//            for (CommunicationRole communicationRole : communication.getCommunicationRole()) {
//                CommunicationRoleId communicationRoleId = new CommunicationRoleId();
//                communicationRoleId.setCommunicationid(communicationRetrieve.getId());
//                communicationRoleId.setRolecode(communicationRole.getRolecode());
//                List<Role> role = externalService.checkRoleAvailability(communicationRole, identity);
//                for (Role roleDB : role) {
//                    communicationRole.setRolename(roleDB.getRolename());
//                }
//                communicationRole.setId(communicationRoleId);
//                Date date = new Date();
//                communicationRole.setCreatedDate(date);
//                communicationRole.setCreatedBy(identity.getUserid());
//                communicationRole.setActive(true);
//                communicationRoleList.add(communicationRole);
//                communicationRoleRepository.save(communicationRole);
//            }
//            return communication;
//        }
//        return null;
//    }
}
