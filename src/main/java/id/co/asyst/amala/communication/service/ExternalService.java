package id.co.asyst.amala.communication.service;

import id.co.asyst.amala.communication.model.CommunicationRole;
import id.co.asyst.amala.core.exception.ValidationException;
//import id.co.asyst.amala.user.model.Role;
//import id.co.asyst.amala.user.payload.role.RoleListResponse;
//import id.co.asyst.amala.user.payload.role.RoleParameter;
//import id.co.asyst.amala.user.payload.role.RoleRequest;
import id.co.asyst.commons.core.component.RESTService;
import id.co.asyst.commons.core.payload.Identity;
import id.co.asyst.commons.core.payload.Status;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Mar 27, 2019
 * @since 4.0
 */
@Component
public class ExternalService {

    private RESTService service;

    @Value("${url.retrieve.role}")
    private String urlRoleRetrieve;

    public ExternalService(RESTService service) {
        this.service = service;
    }

//    public List<Role> checkRoleAvailability(CommunicationRole communicationRole, Identity identity) {
//        RoleParameter roleParameter = new RoleParameter();
//        roleParameter.addCriteria("rolecode", String.valueOf(communicationRole.getRolecode()));
//        RoleRequest roleRequest = new RoleRequest();
//        roleRequest.setParameter(roleParameter);
//        roleRequest.setIdentity(identity);
//        RoleListResponse roleResponse;
//        try {
//            roleResponse = (RoleListResponse) service.post(urlRoleRetrieve, roleRequest, RoleListResponse.class);
//        } catch (Exception e) {
//            throw new ValidationException(Status.INVALID("Error Data"));
//        }
//        return roleResponse.getResult();
//    }
}
