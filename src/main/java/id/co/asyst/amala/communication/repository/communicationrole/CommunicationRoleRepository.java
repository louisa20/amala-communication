/*
 * $Id$
 *
 * Copyright (c) 2018 Aero Systems Indonesia, PT.
 * All rights reserved.
 *
 * AERO SYSTEMS INDONESIA PROPRIETARY/CONFIDENTIAL. Use is subject to
 * license terms.
 */
package id.co.asyst.amala.communication.repository.communicationrole;

import id.co.asyst.amala.communication.model.CommunicationRole;
import id.co.asyst.commons.core.repository.BaseRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Mar 27, 2019
 * @since 4.0
 */
@Repository
public interface CommunicationRoleRepository extends BaseRepository<CommunicationRole, String>, CommunicationRoleRepositoryCustom {
    @Query("from CommunicationRole a where UPPER(a.communication.id) = UPPER(:communicationid)")
    List<CommunicationRole> findByCommunicationid(@Param("communicationid") String communicationid);

    @Transactional
    @Modifying
    @Query("DELETE from CommunicationRole a where a.communication.id = :id")
    void deleteCommunicationRoleByCommunicationId(String id);
}
